import numpy as np

def x_y(name_file):
    content = np.loadtxt(name_file, skiprows=2)
    for y in range(0, content.shape[0]):
        for x in range(0, content.shape[1]):
            if content[y, x] == 1:
                return x, y

x, y = x_y("img1.txt")
x1, y1 = x_y("img2.txt")
print("Смещение по x: ", x1 - x, "Смещение по y: ", y1 - y)
