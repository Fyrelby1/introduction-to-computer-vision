def get_max_length_of_one_subseq(seq_nums: list):
    list_ones = "".join(map(str, seq_nums)).split('0')
    return len(max(list_ones, key=len))
def resolution(name_file):
    with open(name_file) as file:
        content = file.read().split()
    mm = int(content[0])
    del content[0:2]
    pixel = get_max_length_of_one_subseq(content)
    if pixel != 0:
        resolution = mm/pixel
    else: return 0
    return resolution

print(resolution("figure1.txt"))
print(resolution("figure2.txt"))
print(resolution("figure4.txt"))
print(resolution("figure5.txt"))
print(resolution("figure6.txt"))
