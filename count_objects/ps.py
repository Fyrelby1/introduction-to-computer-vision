import numpy as np 
import matplotlib.pyplot as plt 
from skimage.morphology import (binary_erosion, binary_dilation, binary_closing, binary_opening)
from skimage.measure import label

ps = np.load(r"D:\vision\count_objects\psnpy.txt")
labeled = label(ps)
all_ps = len(np.unique(labeled)) - 1
print("Всего: ", all_ps)

mask1 = np.array([
    [1, 1, 1, 1, 1, 1],
     [1, 1, 1, 1, 1, 1],
     [1, 1, 0, 0, 1, 1],
     [1, 1, 0, 0, 1, 1]
])
mask2 = np.array([
    [1, 1, 1, 1],
    [1, 1, 1, 1],
     [1, 1, 0, 0],
     [1, 1, 0, 0],
     [1, 1, 1, 1],
     [1, 1, 1, 1]
])
mask3 = np.array([
    [1, 1, 1, 1],
    [1, 1, 1, 1],
     [0, 0, 1, 1],
     [0, 0, 1, 1],
     [1, 1, 1, 1],
     [1, 1, 1, 1]
])
mask4 = np.array([
    [1, 1, 0, 0, 1, 1],
     [1, 1, 0, 0, 1, 1],
     [1, 1, 1, 1, 1, 1],
     [1, 1, 1, 1, 1, 1]
])
mask5 = np.array([
    [1, 1, 1, 1, 1, 1],
     [1, 1, 1, 1, 1, 1],
     [1, 1, 1, 1, 1, 1],
     [1, 1, 1, 1, 1, 1]
])

rect = binary_erosion(ps, mask5)
labeled_rect = label(rect)
count_rect = len(np.unique(labeled_rect)) - 1
print("Прямоугольники: ", count_rect)

fig1 = binary_erosion(ps, mask1)
labeled1 = label(fig1)
count_fig1 = len(np.unique(labeled1)) - 1
print("Фигура с нижнем вырезом: ", count_fig1 - count_rect)

fig4 = binary_erosion(ps, mask4)
labeled4 = label(fig4)
count_fig4 = len(np.unique(labeled4)) - 1
print("Фигура с верхним вырезом: ", count_fig4 - count_rect)

fig2 = binary_erosion(ps, mask2)
labeled2 = label(fig2)
count_fig2 = len(np.unique(labeled2)) - 1
print("Фигура с правым вырезом: ", count_fig2)

fig3 = binary_erosion(ps, mask3)
labeled3 = label(fig3)
count_fig3 = len(np.unique(labeled3)) - 1
print("Фигура с левым вырезом: ", count_fig3)

plt.imshow(ps)
plt.show()