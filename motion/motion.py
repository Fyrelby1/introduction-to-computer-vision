import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops

figure1 = []
figure2 = []

for i in range(0, 100):
    image = np.load(f'h_{i}.npy')
    labeled = label(image)
    regions = regionprops(labeled)
    if len(regions) == 2:
        regions.sort(key=lambda x: x.area, reverse=True)
        sorted_fig = regions
        figure1.append(regions[0].centroid)
        figure2.append(regions[1].centroid)

figure1 = np.array(figure1)
figure2 = np.array(figure2)

plt.plot(figure1[:, 1], figure1[:, 0], c='green')
plt.plot(figure2[:, 1], figure2[:, 0], c='purple')
plt.title('Trajectory of movement')
plt.show()

