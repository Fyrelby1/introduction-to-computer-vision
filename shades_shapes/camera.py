import cv2
import matplotlib.pyplot as plt
import numpy as np
from skimage.measure import label, regionprops
from skimage.color import rgb2hsv
def count_shapes(image_path):
    image = plt.imread(image_path)
    labeled = label(image.mean(2) > 0)
    hsv = rgb2hsv(image)
    h = hsv[:, :, 0]
    rect_counts = {}
    circle_counts = {}
    for region in regionprops(labeled):
        bbox = region.bbox
        label_val = labeled[bbox[0], bbox[1]]
        if label_val in rect_counts:
            rect_counts[label_val] += 1
        else:
            rect_counts[label_val] = 1
        r = h[bbox[0]:bbox[2], bbox[1]:bbox[3]]
        if label_val in circle_counts:
            circle_counts[label_val].append(len(np.unique(r)) - 1)
        else:
            circle_counts[label_val] = [len(np.unique(r)) - 1]
    re = len(rect_counts)
    ci = len(circle_counts)
    all_count = re + ci
    print("Общее количество фигур на изображении:", all_count)
    print("Количество прямоугольников каждого оттенка:", re)
    print("Количество кругов каждого оттенка:", ci)
    plt.imshow(labeled)
    plt.show()
# Пример использования
count_shapes(r"balls_and_rects.png")
