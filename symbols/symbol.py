import matplotlib.pyplot as plt
import numpy as np
from skimage import draw
from skimage.measure import label, regionprops

def has_vline(arr): #поиск вертикальных линий в букве
    return 1. in arr.mean(0) #0 вертикаль, 1 горизонталь

def recognize(prop): #распределяем символы по местам
    euler_number = prop.euler_number
    if euler_number == -1: #сравниваем с кол-вом дырок
        if has_vline(prop.image):
            return "B"
        else:
            return "8"
    elif euler_number == 0:
        y, x = prop.centroid_local
        y /= prop.image.shape[0]
        x /= prop.image.shape[1]
        if np.isclose(x, y, 0.04):
            if has_vline(prop.image):
                return "P"
            else:
                return "0"
            
        else:
            if has_vline(prop.image):   
                return 'D' 
            else:
                return "A"
    else: 
        if prop.image.mean() == 1.0:
            return "-"
        else: 
            if has_vline(prop.image) and has_vline(prop.image.T):
                return "1"
            else:
                #заливы превращаем в дырки
                tmp = prop.image.copy() 
                tmp[[0, -1], :] = 1 #рамка
                tmp[:, [0, -1]] = 1
                tmp_labeled = label(tmp)
                tmp_prop = regionprops(tmp_labeled)
                tmp_euler = tmp_prop[0].euler_number
                if tmp_euler == -3: #4 дырки поэтому -3
                    return "X"
                elif tmp_euler == -1:
                    return "/"
                else:
                    if prop.eccentricity > 0.5:
                        return "W"
                    else:
                        return "*"
    return "$"



image = plt.imread(r"D:\vision\dz\symbols.png")
image = (image.mean(2))
image = image > 0
labeled = label(image)


# print(labeled.max()) #всего

props = regionprops(labeled) #вырезает букву отдельно 
result = {}
for prop in props:
    symbol = recognize(prop)
    if symbol not in result:
        result[symbol] = 0
    result[symbol] += 1
print(result)

#считаем как в процентах
#print((1 - result.get("$$", 0) / labeled.max()) * 100, "%") #процент всего


# print(props[0].euler_number) #/ 1 это ноль дырок
# print(props[1].euler_number) #B -1 это две дырки
# print(props[25].euler_number) #О 0 это одна дырка

plt.imshow(labeled)
plt.show()

